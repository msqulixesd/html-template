import $ from 'jquery';
import { __debug } from './debug';

/*Import app styles*/
import '../styles/index.scss';

import app from './app.js';

$(() => {
  const ver = $.fn.jquery;
  __debug(`jQuery ${ver} bundled`);
});

app.init();
