const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const baseConfig = require('./index');

const config = {
  module: {
    rules: [{
      test: /\.scss$/,
      exclude: [/node_modules/],
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader',
          options: {
            minimize: false
          }
        },
          'sass-loader'
        ]
      })
    }]
  }
};

module.exports = merge(baseConfig, config);