var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');

module.exports = {

  context: path.resolve('..', __dirname, 'src'),

  devtool: 'cheap-module-eval-source-map',

  entry: {
    'main': path.resolve('src', 'js', 'index.js'),
    'vendor': path.resolve('src', 'js', 'vendor.js'),
  },

  output: {
    path: path.resolve(__dirname, '..', 'dist'),
    filename: '[name].js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'es2016', 'stage-0']
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        exclude: [/node_modules/],
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
        },
      }
    ]
  },

  plugins: [
    new ExtractTextPlugin('styles.css'),
    new webpack.optimize.CommonsChunkPlugin({
      name: ['main', 'vendor']
    }),
    new HtmlWebpackPlugin({
      template: '!!ejs-loader!src/templates/index.html',
      filename: 'index.html',
      hash: true,
      cache: false
    }),
    new HtmlWebpackPlugin({
      template: '!!ejs-loader!src/templates/example.html',
      filename: 'example.html',
      hash: true,
      cache: false
    }),
    new webpack.ProvidePlugin({
      /*$: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"*/
    })
  ]
};