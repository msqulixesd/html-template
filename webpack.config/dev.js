const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const merge = require('webpack-merge');
const baseConfig = require('./index');
const path = require('path');

const devServerConfig = {
  entry: {
    hot: 'webpack-hot-middleware/client?http://localhost:3000',
  },
  output: {
    sourceMapFilename: '[file].map'
  },
  module: {
    rules: [{
      test: /\.scss$/,
      exclude: [/node_modules/],
      use: ['style-loader', 'css-loader', 'sass-loader']
    }]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ]
};

const config = merge(baseConfig, devServerConfig);

const app = express();
app.use(express.static(path.resolve(__dirname, '..', 'dist')));
const webpackCompiler = webpack(config);
const wpmw = webpackMiddleware(webpackCompiler,{});
app.use(wpmw);

const wphmw = webpackHotMiddleware(webpackCompiler);
app.use(wphmw);

app.listen(3000, () => {
  console.log('Example app listening on port 3000!')
});
